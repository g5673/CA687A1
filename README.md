# CA687 Assignment 1

Graham Comerford, Mary Monaghan

## Description
Cryptocurrency price and trading indicator dashboard.

## Instructions
1. Create Google Storage Bucket 'ca687-bucket'.
2. Clone repository and move contents into ca687-bucket
3. Run provision_cluster.sh in Google cloud shell to create Dataproc Cluster.
4. SSH into the Dataproc Cluster and run download_data.sh to update the crypto csv files
5. Use hive_commands.hql to create the database, tables and to run the queries.
6. Use save_and_move_csv.sh to save the tables as csv files and move them into the storage bucket.
7. Refresh data in Datastudio to display the updated data.

## DataStudio Link
https://datastudio.google.com/reporting/0c5385b8-1eed-4cd4-80e4-0f56aef35d50

## Video Demo
https://www.youtube.com/watch?v=vHc47qxlUAU

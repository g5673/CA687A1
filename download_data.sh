#!/bin/sh
wget https://www.cryptodatadownload.com/cdd/Binance_BTCUSDT_d.csv
wget https://www.cryptodatadownload.com/cdd/Binance_ETHUSDT_d.csv
wget https://www.cryptodatadownload.com/cdd/Binance_LTCUSDT_d.csv
wget https://www.cryptodatadownload.com/cdd/Binance_DOGEUSDT_d.csv
wget https://www.cryptodatadownload.com/cdd/Binance_NEOUSDT_d.csv
wget https://www.cryptodatadownload.com/cdd/Binance_BNBUSDT_d.csv
wget https://www.cryptodatadownload.com/cdd/Binance_XRPUSDT_d.csv
wget https://www.cryptodatadownload.com/cdd/Binance_LINKUSDT_d.csv
wget https://www.cryptodatadownload.com/cdd/Binance_EOSUSDT_d.csv
wget https://www.cryptodatadownload.com/cdd/Binance_TRXUSDT_d.csv
wget https://www.cryptodatadownload.com/cdd/Binance_ETCUSDT_d.csv
wget https://www.cryptodatadownload.com/cdd/Binance_XLMUSDT_d.csv
wget https://www.cryptodatadownload.com/cdd/Binance_ZECUSDT_d.csv
wget https://www.cryptodatadownload.com/cdd/Binance_ADAUSDT_d.csv
wget https://www.cryptodatadownload.com/cdd/Binance_QTUMUSDT_d.csv
wget https://www.cryptodatadownload.com/cdd/Binance_DASHUSDT_d.csv
wget https://www.cryptodatadownload.com/cdd/Binance_XMRUSDT_d.csv
wget https://www.cryptodatadownload.com/cdd/Binance_BTTUSDT_d.csv

wget https://www.cryptodatadownload.com/cdd/BTCUSDT_Binance_futures_data_day.csv
wget https://www.cryptodatadownload.com/cdd/ETHUSDT_Binance_futures_data_day.csv
wget https://www.cryptodatadownload.com/cdd/LTCUSDT_Binance_futures_data_day.csv
wget https://www.cryptodatadownload.com/cdd/LINKUSDT_Binance_futures_data_day.csv
wget https://www.cryptodatadownload.com/cdd/BNBUSDT_Binance_futures_data_day.csv
wget https://www.cryptodatadownload.com/cdd/XRPUSDT_Binance_futures_data_day.csv
wget https://www.cryptodatadownload.com/cdd/EOSUSDT_Binance_futures_data_day.csv
wget https://www.cryptodatadownload.com/cdd/TRXUSDT_Binance_futures_data_day.csv
wget https://www.cryptodatadownload.com/cdd/NEOUSDT_Binance_futures_data_day.csv
wget https://www.cryptodatadownload.com/cdd/ETCUSDT_Binance_futures_data_day.csv
wget https://www.cryptodatadownload.com/cdd/XLMUSDT_Binance_futures_data_day.csv
wget https://www.cryptodatadownload.com/cdd/BATUSDT_Binance_futures_data_day.csv
wget https://www.cryptodatadownload.com/cdd/DASHUSDT_Binance_futures_data_day.csv
wget https://www.cryptodatadownload.com/cdd/ZECUSDT_Binance_futures_data_day.csv
wget https://www.cryptodatadownload.com/cdd/XMRUSDT_Binance_futures_data_day.csv
wget https://www.cryptodatadownload.com/cdd/ADAUSDT_Binance_futures_data_day.csv


gsutil mv Binance_BTCUSDT_d.csv gs://ca687-bucket/data/binance_price_data/Binance_BTCUSDT_d.csv
gsutil mv Binance_ETHUSDT_d.csv gs://ca687-bucket/data/binance_price_data/Binance_ETHUSDT_d.csv
gsutil mv Binance_LTCUSDT_d.csv gs://ca687-bucket/data/binance_price_data/Binance_LTCUSDT_d.csv
gsutil mv Binance_DOGEUSDT_d.csv gs://ca687-bucket/data/binance_price_data/Binance_DOGEUSDT_d.csv
gsutil mv Binance_NEOUSDT_d.csv gs://ca687-bucket/data/binance_price_data/Binance_NEOUSDT_d.csv
gsutil mv Binance_BNBUSDT_d.csv gs://ca687-bucket/data/binance_price_data/Binance_BNBUSDT_d.csv
gsutil mv Binance_XRPUSDT_d.csv gs://ca687-bucket/data/binance_price_data/Binance_XRPUSDT_d.csv
gsutil mv Binance_LINKUSDT_d.csv gs://ca687-bucket/data/binance_price_data/Binance_LINKUSDT_d.csv
gsutil mv Binance_EOSUSDT_d.csv gs://ca687-bucket/data/binance_price_data/Binance_EOSUSDT_d.csv
gsutil mv Binance_TRXUSDT_d.csv gs://ca687-bucket/data/binance_price_data/Binance_TRXUSDT_d.csv
gsutil mv Binance_ETCUSDT_d.csv gs://ca687-bucket/data/binance_price_data/Binance_ETCUSDT_d.csv
gsutil mv Binance_XLMUSDT_d.csv gs://ca687-bucket/data/binance_price_data/Binance_XLMUSDT_d.csv
gsutil mv Binance_ZECUSDT_d.csv gs://ca687-bucket/data/binance_price_data/Binance_ZECUSDT_d.csv
gsutil mv Binance_ADAUSDT_d.csv gs://ca687-bucket/data/binance_price_data/Binance_ADAUSDT_d.csv
gsutil mv Binance_QTUMUSDT_d.csv gs://ca687-bucket/data/binance_price_data/Binance_QTUMUSDT_d.csv
gsutil mv Binance_DASHUSDT_d.csv gs://ca687-bucket/data/binance_price_data/Binance_DASHUSDT_d.csv
gsutil mv Binance_XMRUSDT_d.csv gs://ca687-bucket/data/binance_price_data/Binance_XMRUSDT_d.csv
gsutil mv Binance_BTTUSDT_d.csv gs://ca687-bucket/data/binance_price_data/Binance_BTTUSDT_d.csv


gsutil mv  BTCUSDT_Binance_futures_data_day.csv gs://ca687-bucket/data/binance_future_prices/BTCUSDT_Binance_futures_data_day.csv
gsutil mv  ETHUSDT_Binance_futures_data_day.csv gs://ca687-bucket/data/binance_future_prices/ETHUSDT_Binance_futures_data_day.csv
gsutil mv  LTCUSDT_Binance_futures_data_day.csv gs://ca687-bucket/data/binance_future_prices/LTCUSDT_Binance_futures_data_day.csv
gsutil mv  LINKUSDT_Binance_futures_data_day.csv gs://ca687-bucket/data/binance_future_prices/LINKUSDT_Binance_futures_data_day.csv
gsutil mv  BNBUSDT_Binance_futures_data_day.csv gs://ca687-bucket/data/binance_future_prices/BNBUSDT_Binance_futures_data_day.csv
gsutil mv  XRPUSDT_Binance_futures_data_day.csv gs://ca687-bucket/data/binance_future_prices/XRPUSDT_Binance_futures_data_day.csv
gsutil mv  EOSUSDT_Binance_futures_data_day.csv gs://ca687-bucket/data/binance_future_prices/EOSUSDT_Binance_futures_data_day.csv
gsutil mv  TRXUSDT_Binance_futures_data_day.csv gs://ca687-bucket/data/binance_future_prices/TRXUSDT_Binance_futures_data_day.csv
gsutil mv  NEOUSDT_Binance_futures_data_day.csv gs://ca687-bucket/data/binance_future_prices/NEOUSDT_Binance_futures_data_day.csv
gsutil mv  ETCUSDT_Binance_futures_data_day.csv gs://ca687-bucket/data/binance_future_prices/ETCUSDT_Binance_futures_data_day.csv
gsutil mv  XLMUSDT_Binance_futures_data_day.csv gs://ca687-bucket/data/binance_future_prices/XLMUSDT_Binance_futures_data_day.csv
gsutil mv  BATUSDT_Binance_futures_data_day.csv gs://ca687-bucket/data/binance_future_prices/BATUSDT_Binance_futures_data_day.csv
gsutil mv  DASHUSDT_Binance_futures_data_day.csv gs://ca687-bucket/data/binance_future_prices/DASHUSDT_Binance_futures_data_day.csv
gsutil mv  ZECUSDT_Binance_futures_data_day.csv gs://ca687-bucket/data/binance_future_prices/ZECUSDT_Binance_futures_data_day.csv
gsutil mv  XMRUSDT_Binance_futures_data_day.csv gs://ca687-bucket/data/binance_future_prices/XMRUSDT_Binance_futures_data_day.csv
gsutil mv  ADAUSDT_Binance_futures_data_day.csv gs://ca687-bucket/data/binance_future_prices/ADAUSDT_Binance_futures_data_day.csv

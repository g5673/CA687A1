#!/bin/bash

gcloud sql instances create ca687cluster-sql \
    --tier db-n1-standard-1 \
    --activation-policy=ALWAYS \
    --region europe-west2

HIVE_DATA_BUCKET=ca687-bucket
PROJECT_ID=ca687-341310
REGION=europe-west2
INSTANCE_NAME=ca687cluster-sql
CLUSTER_NAME=ca687-cluster

gcloud dataproc clusters create ${CLUSTER_NAME} \
--region ${REGION} \
--single-node \
--scopes sql-admin \
--initialization-actions gs://goog-dataproc-initialization-actions-${REGION}/cloud-sql-proxy/cloud-sql-proxy.sh \
--properties hive:hive.metastore.warehouse.dir=gs://${HIVE_DATA_BUCKET}/hive-warehouse \
--metadata "hive-metastore-instance=${PROJECT_ID}:${REGION}:${INSTANCE_NAME}"

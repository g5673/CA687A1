#!/bin/sh

hive -e 'select * from database_coins.binance_price_data' | sed 's/[\t]/,/g'  > binance_prices.csv
gsutil mv binance_prices.csv gs://ca687-bucket/output/binance/binance_prices.csv

hive -e 'select * from database_coins.binance_futures_data' | sed 's/[\t]/,/g'  > binance_futures.csv
gsutil mv binance_futures.csv gs://ca687-bucket/output/binance/binance_futures.csv

hive -e 'select * from database_coins.binance_candles' | sed 's/[\t]/,/g'  > binance_candles.csv
gsutil mv binance_candles.csv gs://ca687-bucket/output/binance/binance_candles.csv

hive -e 'select * from database_coins.gold_price_data' | sed 's/[\t]/,/g'  > gold_price_data.csv
gsutil mv gold_price_data.csv gs://ca687-bucket/output/gold_price_data.csv

hive -e 'select * from database_coins.oil_price_data' | sed 's/[\t]/,/g'  > oil_price_data.csv
gsutil mv oil_price_data.csv gs://ca687-bucket/output/oil_price_data.csv

hive -e 'select * from database_coins.rs_rsi' | sed 's/[\t]/,/g'  > rs_rsi.csv
gsutil mv rs_rsi.csv gs://ca687-bucket/output/binance/rs_rsi.csv

hive -e 'select * from database_coins.moving_averages' | sed 's/[\t]/,/g'  > moving_average_15.csv
gsutil mv moving_average_15.csv gs://ca687-bucket/output/binance/moving_average_15.csv

hive -e 'select * from database_coins.moving_average_20' | sed 's/[\t]/,/g'  > moving_average_20.csv
gsutil mv moving_average_20.csv gs://ca687-bucket/output/binance/moving_average_20.csv

hive -e 'select * from database_coins.moving_average_30' | sed 's/[\t]/,/g'  > moving_average_30.csv
gsutil mv moving_average_30.csv gs://ca687-bucket/output/binance/moving_average_30.csv

hive -e 'select * from database_coins.moving_average_50' | sed 's/[\t]/,/g'  > moving_average_50.csv
gsutil mv moving_average_50.csv gs://ca687-bucket/output/binance/moving_average_50.csv

hive -e 'select * from database_coins.moving_average_100' | sed 's/[\t]/,/g'  > moving_average_100.csv
gsutil mv moving_average_100.csv gs://ca687-bucket/output/binance/moving_average_100.csv

hive -e 'select * from database_coins.moving_average_200' | sed 's/[\t]/,/g'  > moving_average_200.csv
gsutil mv moving_average_200.csv gs://ca687-bucket/output/binance/moving_average_200.csv

-- Setup of tables and loading data
CREATE DATABASE IF NOT EXISTS database_coins;
use database_coins;

CREATE TABLE IF NOT EXISTS binance_price_data(a_timestamp BIGINT, 
a_date DATE, 
symbol STRING, 
open DECIMAL(38,20), 
high DECIMAL(38,20), 
low DECIMAL(38,20), 
close DECIMAL(38,20), 
volume_coins DECIMAL(38,20), 
volume_dollars DECIMAL(38,20), 
tradecount INT)
ROW FORMAT delimited
FIELDS TERMINATED BY ','
TBLPROPERTIES("skip.header.line.count"="2");
DESCRIBE binance_price_data;
load data inpath 'gs://ca687-bucket/data/binance_price_data/' into table binance_price_data;
SELECT * FROM binance_price_data;

CREATE TABLE IF NOT EXISTS binance_futures_data(a_timestamp BIGINT, 
a_date DATE, 
symbol STRING, 
open DECIMAL(38,20), 
high DECIMAL(38,20), 
low DECIMAL(38,20), 
close DECIMAL(38,20), 
volume_coins DECIMAL(38,20), 
volume_dollars DECIMAL(38,20), 
tradecount INT)
ROW FORMAT delimited
FIELDS TERMINATED BY ','
TBLPROPERTIES("skip.header.line.count"="2");
DESCRIBE binance_futures_data;
load data inpath 'gs://ca687-bucket/data/binance_future_prices/' into table binance_futures_data;

CREATE TABLE IF NOT EXISTS gold_price_data(s_date DATE, 
close DECIMAL(38,20), 
volume DECIMAL(38,20), 
open DECIMAL(38,20), 
high DECIMAL(38,20), 
low DECIMAL(38,20))
ROW FORMAT delimited
FIELDS TERMINATED BY ','
TBLPROPERTIES("skip.header.line.count"="1");
DESCRIBE gold_price_data;
load data inpath 'gs://ca687-bucket/data/gold_prices/nasdaq_gold.csv' into table gold_price_data;
SELECT * FROM gold_price_data;

CREATE TABLE IF NOT EXISTS oil_price_data(s_date DATE, 
close DECIMAL(38,20), 
volume DECIMAL(38,20), 
open DECIMAL(38,20), 
high DECIMAL(38,20), 
low DECIMAL(38,20))
ROW FORMAT delimited
FIELDS TERMINATED BY ','
TBLPROPERTIES("skip.header.line.count"="1");
DESCRIBE oil_price_data;
load data inpath 'gs://ca687-bucket/data/oil_prices/oil_historical_date_corrected.csv' into table oil_price_data;
SELECT * FROM oil_price_data;


-- Relative Strength Index Calculation
CREATE TABLE close_14
as(SELECT a_date, symbol, close,
(LAG(close, 1) OVER (PARTITION BY symbol ORDER BY a_date)) as lagged_close1,
(LAG(close, 2) OVER (PARTITION BY symbol ORDER BY a_date)) as lagged_close2,
(LAG(close, 3) OVER (PARTITION BY symbol ORDER BY a_date)) as lagged_close3,
(LAG(close, 4) OVER (PARTITION BY symbol ORDER BY a_date)) as lagged_close4,
(LAG(close, 5) OVER (PARTITION BY symbol ORDER BY a_date)) as lagged_close5,
(LAG(close, 6) OVER (PARTITION BY symbol ORDER BY a_date)) as lagged_close6,
(LAG(close, 7) OVER (PARTITION BY symbol ORDER BY a_date)) as lagged_close7,
(LAG(close, 8) OVER (PARTITION BY symbol ORDER BY a_date)) as lagged_close8,
(LAG(close, 9) OVER (PARTITION BY symbol ORDER BY a_date)) as lagged_close9,
(LAG(close, 10) OVER (PARTITION BY symbol ORDER BY a_date)) as lagged_close10,
(LAG(close, 11) OVER (PARTITION BY symbol ORDER BY a_date)) as lagged_close11,
(LAG(close, 12) OVER (PARTITION BY symbol ORDER BY a_date)) as lagged_close12,
(LAG(close, 13) OVER (PARTITION BY symbol ORDER BY a_date)) as lagged_close13,
(LAG(close, 14) OVER (PARTITION BY symbol ORDER BY a_date)) as lagged_close14
FROM binance_price_data);

CREATE TABLE close_gains_losses
as(SELECT a_date, symbol, close,
CASE
    WHEN lagged_close1 > close THEN lagged_close1 - close
    ELSE 0
END AS gain1,
CASE
    WHEN lagged_close2 > lagged_close1 THEN lagged_close2 - lagged_close1
    ELSE 0
END AS gain2,
CASE
    WHEN lagged_close3 > lagged_close2 THEN lagged_close3 - lagged_close2
    ELSE 0
END AS gain3,
CASE
    WHEN lagged_close4 > lagged_close3 THEN lagged_close4 - lagged_close3
    ELSE 0
END AS gain4,
CASE
    WHEN lagged_close5 > lagged_close4 THEN lagged_close5 - lagged_close4
    ELSE 0
END AS gain5,
CASE
    WHEN lagged_close6 > lagged_close5 THEN lagged_close6 - lagged_close5
    ELSE 0
END AS gain6,
CASE
    WHEN lagged_close7 > lagged_close6 THEN lagged_close7 - lagged_close6
    ELSE 0
END AS gain7,
CASE
    WHEN lagged_close8 > lagged_close7 THEN lagged_close8 - lagged_close7
    ELSE 0
END AS gain8,
CASE
    WHEN lagged_close9 > lagged_close8 THEN lagged_close9 - lagged_close8
    ELSE 0
END AS gain9,
CASE
    WHEN lagged_close10 > lagged_close9 THEN lagged_close10 - lagged_close9
    ELSE 0
END AS gain10,
CASE
    WHEN lagged_close11 > lagged_close10 THEN lagged_close11 - lagged_close10
    ELSE 0
END AS gain11,
CASE
    WHEN lagged_close12 > lagged_close11 THEN lagged_close12 - lagged_close11
    ELSE 0
END AS gain12,
CASE
    WHEN lagged_close13 > lagged_close12 THEN lagged_close13 - lagged_close12
    ELSE 0
END AS gain13,
CASE
    WHEN lagged_close14 > lagged_close13 THEN lagged_close14 - lagged_close13
    ELSE 0
END AS gain14,
CASE
    WHEN lagged_close1 < close THEN close - lagged_close1
    ELSE 0
END AS loss1,
CASE
    WHEN lagged_close2 < lagged_close1 THEN lagged_close1 - lagged_close2
    ELSE 0
END AS loss2,
CASE
    WHEN lagged_close3 < lagged_close2 THEN lagged_close2 - lagged_close3
    ELSE 0
END AS loss3,
CASE
    WHEN lagged_close4 < lagged_close3 THEN lagged_close3 - lagged_close4
    ELSE 0
END AS loss4,
CASE
    WHEN lagged_close5 < lagged_close4 THEN lagged_close4 - lagged_close5
    ELSE 0
END AS loss5,
CASE
    WHEN lagged_close6 < lagged_close5 THEN lagged_close5 - lagged_close6
    ELSE 0
END AS loss6,
CASE
    WHEN lagged_close7 < lagged_close6 THEN lagged_close6 - lagged_close7
    ELSE 0
END AS loss7,
CASE
    WHEN lagged_close8 < lagged_close7 THEN lagged_close7 - lagged_close8
    ELSE 0
END AS loss8,
CASE
    WHEN lagged_close9 < lagged_close8 THEN lagged_close8 - lagged_close9
    ELSE 0
END AS loss9,
CASE
    WHEN lagged_close10 < lagged_close9 THEN lagged_close9 - lagged_close10
    ELSE 0
END AS loss10,
CASE
    WHEN lagged_close11 < lagged_close10 THEN lagged_close10 - lagged_close11
    ELSE 0
END AS loss11,
CASE
    WHEN lagged_close12 < lagged_close11 THEN lagged_close11 - lagged_close12
    ELSE 0
END AS loss12,
CASE
    WHEN lagged_close13 < lagged_close12 THEN lagged_close12 - lagged_close13
    ELSE 0
END AS loss13,
CASE
    WHEN lagged_close14 < lagged_close13 THEN lagged_close13 - lagged_close14
    ELSE 0
END AS loss14
FROM close_14);

CREATE TABLE avg_gain_loss
AS(SELECT a_date, symbol, close,
(gain1 + gain2 + gain3 + gain4 + gain5 + gain6 + gain7 + gain8 + gain9 + gain10 + gain11 + gain12 + gain13 + gain14)/14 AS gain_avg,
(loss1 + loss2 + loss3 + loss4 + loss5 + loss6 + loss7 + loss8 + loss9 + loss10 + loss11 + loss12 + loss13 + loss14)/14 AS loss_avg
FROM close_gains_losses);

CREATE TABLE rs_rsi
AS(SELECT a_date, symbol, gain_avg/loss_avg as rs, 100-(100/(1+(gain_avg/loss_avg))) as rsi
FROM avg_gain_loss);


-- Simple Moving Average Calctulations
CREATE TABLE moving_averages(symbol STRING, period_end DATE, moving_average_15 DECIMAL(38,20))
ROW FORMAT delimited
FIELDS TERMINATED BY ',';

insert into table moving_averages
SELECT symbol, a_date, 
avg(high) over wind_15 as moving_average_15
FROM database_coins.binance_price_data 
WINDOW wind_15 AS (PARTITION BY symbol ORDER BY a_date rows between 14 preceding and current row);

CREATE TABLE moving_average_20(symbol STRING, period_end DATE, moving_average_20 DECIMAL(38,20))
ROW FORMAT delimited
FIELDS TERMINATED BY ',';

insert into table moving_average_20
SELECT symbol, a_date, 
avg(high) over wind_20 as moving_average_20
FROM database_coins.binance_price_data 
WINDOW wind_20 AS (PARTITION BY symbol ORDER BY a_date rows between 19 preceding and current row);

CREATE TABLE moving_average_30(symbol STRING, period_end DATE, moving_average_30 DECIMAL(38,20))
ROW FORMAT delimited
FIELDS TERMINATED BY ',';

insert into table moving_average_30
SELECT symbol, a_date, 
avg(high) over wind_30 as moving_average_30
FROM database_coins.binance_price_data 
WINDOW wind_30 AS (PARTITION BY symbol ORDER BY a_date rows between 29 preceding and current row);

CREATE TABLE moving_average_50(symbol STRING, period_end DATE, moving_average_50 DECIMAL(38,20))
ROW FORMAT delimited
FIELDS TERMINATED BY ',';

insert into table moving_average_50
SELECT symbol, a_date, 
avg(high) over wind_50 as moving_average_50
FROM database_coins.binance_price_data 
WINDOW wind_50 AS (PARTITION BY symbol ORDER BY a_date rows between 49 preceding and current row);

CREATE TABLE moving_average_100(symbol STRING, period_end DATE, moving_average_100 DECIMAL(38,20))
ROW FORMAT delimited
FIELDS TERMINATED BY ',';

insert into table moving_average_100
SELECT symbol, a_date, 
avg(high) over wind_100 as moving_average_100
FROM database_coins.binance_price_data 
WINDOW wind_100 AS (PARTITION BY symbol ORDER BY a_date rows between 99 preceding and current row);

CREATE TABLE moving_average_200(symbol STRING, period_end DATE, moving_average_200 DECIMAL(38,20))
ROW FORMAT delimited
FIELDS TERMINATED BY ',';

insert into table moving_average_200
SELECT symbol, a_date, 
avg(high) over wind_200 as moving_average_200
FROM database_coins.binance_price_data 
WINDOW wind_200 AS (PARTITION BY symbol ORDER BY a_date rows between 199 preceding and current row);



